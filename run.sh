export VOLUME_NAME="gpt-volume"
export VOLUME_DIR="/media/${VOLUME_NAME}"
export PRETRAINED_MODEL="EleutherAI/gpt-neo-2.7B"
export TRANSFORMERS_CACHE="${VOLUME_DIR}/model_cache"
export MODEL_OUTPUT_DIR="${VOLUME_DIR}/finetuned_model"
export PYTHON_VERSION="$(python -V | sed -nr 's/.* ([0-9]\.[0-9]).*/\1/p')"
export PATH="$PATH:/home/ubuntu/.local/bin"
export WANDB_DISABLED="true"

set -e

sudo apt-get update && \
  sudo apt-get install -y python${PYTHON_VERSION}-dev

pip3 install --user -r requirements.txt

python3 gpt_neo_xl_deepspeed.py 2>&1 > /proc/1/fd/1
python3 text_gen.py 2>&1 > /proc/1/fd/1
