import os
import gc
os.environ['MASTER_ADDR'] = 'localhost'
os.environ['MASTER_PORT'] = '9994'
os.environ['RANK'] = "0"
os.environ['LOCAL_RANK'] = "0"
os.environ['WORLD_SIZE'] = "1"
import pandas as pd
import torch
from torch.utils.data import Dataset, random_split
from transformers import GPT2Tokenizer, TrainingArguments, Trainer, GPTNeoForCausalLM
from transformers.trainer_utils import get_last_checkpoint, is_main_process

PRETRAINED_MODEL = os.getenv('PRETRAINED_MODEL')
MODEL_OUTPUT_DIR = os.getenv('MODEL_OUTPUT_DIR')
VOLUME_DIR = os.getenv('VOLUME_DIR')

torch.manual_seed(42)
tokenizer = GPT2Tokenizer.from_pretrained("{}".format(PRETRAINED_MODEL), bos_token='<|endoftext|>',
                                          eos_token='<|endoftext|>', pad_token='<|pad|>')
model = GPTNeoForCausalLM.from_pretrained("{}".format(PRETRAINED_MODEL)).half().to("cuda")
model.resize_token_embeddings(len(tokenizer))
descriptions = pd.read_csv('netflix_titles.csv')['description']
max_length = max([len(tokenizer.encode(description)) for description in descriptions])
print("Max length: {}".format(max_length))

class NetflixDataset(Dataset):
    def __init__(self, txt_list, tokenizer, max_length):
        self.input_ids = []
        self.attn_masks = []
        self.labels = []
        for txt in txt_list:
            encodings_dict = tokenizer('<|endoftext|>' + txt + '<|endoftext|>', truncation=True,
                                       max_length=max_length, padding="max_length")
            self.input_ids.append(torch.tensor(encodings_dict['input_ids']))
            self.attn_masks.append(torch.tensor(encodings_dict['attention_mask']))

    def __len__(self):
        return len(self.input_ids)

    def __getitem__(self, idx):
        return self.input_ids[idx], self.attn_masks[idx]


dataset = NetflixDataset(descriptions, tokenizer, max_length=max_length)
train_size = int(0.9 * len(dataset))
train_dataset, eval_dataset = random_split(dataset, [train_size, len(dataset) - train_size])

gc.collect()
#
torch.cuda.empty_cache()

"""
num_train_epochs=1 - Total number of training epochs to perform (if not an integer, will perform the decimal part percents of the last epoch before stopping training).
logging_steps=100 - Number of update steps between two logs if logging_strategy="steps".
save_steps=500 - Number of updates steps before two checkpoint saves if save_strategy="steps".
per_device_train_batch_size=7 - The batch size per GPU/TPU core/CPU for training.
per_device_eval_batch_size=7 - The batch size per GPU/TPU core/CPU for evaluation.
warmup_steps=100 Number of steps used for a linear warmup from 0 to learning_rate. Overrides any effect of warmup_ratio.
weight_decay=0.01 - The weight decay to apply (if not zero) to all layers except all bias and LayerNorm weights in AdamW optimizer.
fp16=True - Whether to use 16-bit (mixed) precision training instead of 32-bit training.
gradient_accumulation_steps=1 - Number of updates steps to accumulate the gradients for, before performing a backward/update pass.
learning_rate=5e-06 - The initial learning rate for AdamW optimizer.
"""
training_args = TrainingArguments(output_dir='{}'.format(MODEL_OUTPUT_DIR), num_train_epochs=1, logging_steps=500, save_steps=200,
                                  per_device_train_batch_size=2, per_device_eval_batch_size=2, warmup_steps=100,
                                  weight_decay=0.01, fp16=True, gradient_accumulation_steps=1, learning_rate=5e-5,
                                  logging_dir='{}/logs'.format(VOLUME_DIR), deepspeed='./ds_config.json')
trainer = Trainer(model=model, args=training_args, train_dataset=train_dataset,
                  eval_dataset=eval_dataset, data_collator=lambda data: {'input_ids': torch.stack([f[0] for f in data]),
                                                                        'attention_mask': torch.stack([f[1] for f in data]),
                                                                        'labels': torch.stack([f[0] for f in data])})
trainer.train()
trainer.save_model()
trainer.save_state()
tokenizer.save_pretrained('{}'.format(MODEL_OUTPUT_DIR))
