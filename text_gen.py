
from transformers import GPT2Tokenizer, GPTNeoForCausalLM
import argparse,os

PRETRAINED_MODEL = os.getenv('MODEL_OUTPUT_DIR')

tokenizer = GPT2Tokenizer.from_pretrained("{}".format(PRETRAINED_MODEL), bos_token='<|endoftext|>',
                                          eos_token='<|endoftext|>', pad_token='<|pad|>')
model = GPTNeoForCausalLM.from_pretrained("{}".format(PRETRAINED_MODEL)).half().to("cuda")

"""
do_sample=True - Whether or not to use sampling ; use greedy decoding otherwise.
top_k=50 - The number of highest probability vocabulary tokens to keep for top-k-filtering.
max_length=70 - The maximum length of the sequence to be generated.
top_p=0.95 - If set to float < 1, only the most probable tokens with probabilities that add up to top_p or higher are kept for generation.
temperature=1.9 - The value used to module the next token probabilities.
num_return_sequences=200 - The number of independently computed returned sequences for each element in the batch.
"""
generated = tokenizer("<|endoftext|> ", return_tensors="pt").input_ids.cuda()
sample_outputs = model.generate(generated, do_sample=True, top_k=50,
                                max_length=100, top_p=0.95, temperature=1.9, num_return_sequences=20)
for i, sample_output in enumerate(sample_outputs):
    print("{}: {}".format(i, tokenizer.decode(sample_output, skip_special_tokens=True)))
